const User_gameController = require("./user_game");
const UserbiodataController = require("./userbiodata");
const UserhistoriesController = require("./userhistories");

module.exports = {
  User_gameController,
  UserbiodataController,
  UserhistoriesController,
};
