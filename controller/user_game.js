// const { where } = require("sequelize/types");
const { User_games } = require("../models");

class User_gameController {
  // View
  static async HomeView(req, res, next) {
    // res.send("");
    const data = await User_games.findAll();
    let dataArr = data.map((el) => el.dataValues);
    res.render("user_games", { data: dataArr });
  }

  // static Viewch3(req, res, next) {
  //   // res.send("ch3");
  //   console.log("==ch3==");
  //   res.render("ch3");
  // }
  // static Viewch4(req, res, next) {
  //   // res.send("ch4");
  //   console.log("==ch4==");
  //   res.render("ch4");
  // }

  static async createUser_game(req, res, next) {
    try {
      const { Username, Password } = req.body;
      const _payload = {
        Username,
        Password,
      };
      const data = await User_games.create(_payload);
      // untuk mengambil test get data akan sering digunakan juga
      // console.log(data.datValues.Name,"<<<<<<<< DATA CREATE");
      res.status(201).json(data);
    } catch (error) {
      console.log(error);
    }
  }

  static async getUser_games(req, res, next) {
    try {
      const data = await User_games.findAll();
      // let dataArr = data.map((el) => el.dataValues);
      // res.render("user_games.ejs", { data: dataArr });
      res.status(200).json(data);
    } catch (error) {
      console.log(error);
    }
  }

  static async getUser_game(req, res, next) {
    try {
      const id = req.params.id; //untuk params id data
      const data = await User_games.findOne({
        where: {
          id: id, //where untuk filtering id
        },
      });
      res.status(200).json(data);
    } catch (error) {
      console.log(error);
    }
  }
  static async editUser_game(req, res, next) {
    try {
      const { Username, Password } = req.body;
      const _payload = {
        Username,
        Password,
      };
      // payload menentukan yang mana yang akan diupdate
      const id = req.params.id;
      const data = await User_games.update(_payload, {
        where: {
          id, //shorthand where untuk filtering id
        },
        returning: true, //mengembalikan nilai data yg diedit
      });
      console.log(data);
      res.status(201).json(data);
    } catch (error) {
      console.log(error);
    }
  }
  static async deleteUser_game(req, res, next) {
    try {
      const id = req.params.id; //mengubah suatu data
      const data = await User_games.destroy({
        where: {
          id,
        },
      });
      // console.log(data);
      if (data) {
        res.status(200).json({
          msg: "Success to delete",
        });
      } else {
        res.status(404).json({
          msg: "Fail to delete",
        });
      }
    } catch (error) {
      console.log(error);
    }
  }
}

module.exports = User_gameController;
