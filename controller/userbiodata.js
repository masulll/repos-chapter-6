const { User_games, Userbiodatas } = require("../models");

class UserbiodataController {
  static async createUserbiodata(req, res, next) {
    try {
      const { Fullname, Email, Usergameid } = req.body;
      const data = await Userbiodatas.create({
        Fullname,
        Email,
        Usergameid,
      });
      res.status(201).json(data);
    } catch (error) {
      console.log(error);
    }
  }
  static async getUserbiodata(req, res, next) {
    try {
      const data = await Userbiodatas.findAll({
        include: [
          {
            model: User_games,
          },
        ],
      });
      res.status(200).json(data);
    } catch (error) {
      console.log(error);
    }
  }
}

module.exports = UserbiodataController;
