const { User_games, Userhistories } = require("../models");

class UserhistoriesController {
  static async createUserhistories(req, res, next) {
    try {
      const { win, lose, user_id } = req.body;
      const data = await Userhistories.create({
        win,
        lose,
        user_id,
      });
      res.status(201).json(data);
    } catch (error) {
      console.log(error);
    }
  }
  static async getUserhistories(req, res, next) {
    try {
      const data = await Userhistories.findAll({
        include: [
          {
            model: User_games,
          },
        ],
      });
      res.status(200).json(data);
    } catch (error) {
      console.log(error);
    }
  }
}

module.exports = UserhistoriesController;
