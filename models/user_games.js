"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class User_games extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      User_games.hasOne(models.Userbiodatas, {
        foreignKey: { name: "Usergameid" },
      });
      User_games.hasMany(models.Userhistories, {
        foreignKey: { name: "user_id" },
      });
    }
  }
  User_games.init(
    {
      Username: DataTypes.STRING,
      Password: DataTypes.STRING,
    },
    {
      sequelize,
      modelName: "User_games",
    }
  );
  return User_games;
};
