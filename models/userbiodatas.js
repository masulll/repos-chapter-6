"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class Userbiodatas extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Userbiodatas.belongsTo(models.User_games, {
        foreignKey: { name: "Usergameid" },
      });
    }
  }
  Userbiodatas.init(
    {
      Fullname: DataTypes.STRING,
      Email: DataTypes.STRING,
      Usergameid: DataTypes.INTEGER,
    },
    {
      sequelize,
      modelName: "Userbiodatas",
    }
  );
  return Userbiodatas;
};
