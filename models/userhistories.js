"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class Userhistories extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Userhistories.belongsTo(models.User_games, {
        foreignKey: { name: "user_id" },
      });
    }
  }
  Userhistories.init(
    {
      win: DataTypes.INTEGER,
      lose: DataTypes.INTEGER,
      user_id: DataTypes.INTEGER,
    },
    {
      sequelize,
      modelName: "Userhistories",
    }
  );
  return Userhistories;
};
