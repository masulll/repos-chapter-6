const route = require("express").Router();
const {
  User_gameController,
  UserbiodataController,
  UserhistoriesController,
} = require("../controller");

// route user_game
route.get("/user_games", User_gameController.HomeView);
route.post("/user_game", User_gameController.createUser_game);
route.get("/user_game", User_gameController.getUser_games);
route.get("/user_game/:id", User_gameController.getUser_game);
route.put("/user_game/:id", User_gameController.editUser_game);
route.delete("/user_game/:id", User_gameController.deleteUser_game);

//route userbiodata
route.post("/userbiodata", UserbiodataController.createUserbiodata);
route.get("/userbiodata", UserbiodataController.getUserbiodata);
//route userhistories
route.post("/userhistories", UserhistoriesController.createUserhistories);
route.get("/userhistories", UserhistoriesController.getUserhistories);

module.exports = route;
